function getCoordinates() {
    return new Promise((resolve, reject) =>
        navigator.geolocation.getCurrentPosition(position => resolve(position.coords), error => reject(error)));
}

function get8DayForecast(latitude, longitude) {
    return fetch('https://api.openweathermap.org/data/2.5/onecall?' +
        `lat=${latitude}&lon=${longitude}&exclude=current,minutely,hourly,alerts` +
        '&appid=32a40352b94f9f112f73bc2f2cd13765&units=metric') //todo: remove api key
        .then(response => response.json())
        .then(json => json.daily)
        .catch(error => console.log('Error retrieving the forecast', error));
}

function dateHeading(date) {
    const h4 = document.createElement("h4");
    h4.innerText = date.toDateString();
    return h4;
}

function weatherFigure(day) {
    const weather = day.weather[0];
    const figure = document.createElement("figure");
    const imgElement = document.createElement('img');
    imgElement.src = `http://openweathermap.org/img/wn/${weather.icon}@2x.png`;
    const figCaption = document.createElement("figcaption");
    figCaption.innerText = weather.description;
    figure.appendChild(imgElement);
    figure.appendChild(figCaption);
    return figure;
}

function temperaturesTable(day) {
    const table = document.createElement('table');
    table.className = 'temperaturesTable';
    table.innerHTML = `<tr><td>Morning: </td><td>${day.temp.morn} &deg;C</td></tr>
                       <tr><td>Day: </td><td>${day.temp.day} &deg;C</td></tr>
                       <tr><td>Evening: </td><td>${day.temp.eve} &deg;C</td></tr>
                       <tr><td>Night: </td><td>${day.temp.night} &deg;C</td></tr>`;
    return table;
}

function forecastPerDay(forecast) {
    let forecastPerDay = document.createDocumentFragment();
    let date = new Date();
    for (day of forecast) {
        const div = document.createElement("div");
        div.className = "dayForecast";
        div.appendChild(dateHeading(date));
        div.appendChild(weatherFigure(day));
        div.appendChild(temperaturesTable(day));
        forecastPerDay.appendChild(div);
        date.setDate(date.getDate() + 1);
    }
    return forecastPerDay;
}

async function populateForecastContainer() {
    const {latitude, longitude} = await getCoordinates();
    const forecast = await get8DayForecast(latitude, longitude);
    const forecastContainer = document.querySelector('#forecastContainer');
    forecastContainer.appendChild(forecastPerDay(forecast));
}

document.addEventListener('DOMContentLoaded', () => populateForecastContainer());



