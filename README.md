# My Weather

Simple html/js/css page for displaying the 8 day weather forecast for the user's location.

* Uses the [Geolocation Web API](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/getCurrentPosition) to retrieve the user's coordinates; latitude and longitude.

* Uses the [openweathermap.org REST API](https://openweathermap.org/api) for obtaining the forecast given the latitude and longitute coordinates.
